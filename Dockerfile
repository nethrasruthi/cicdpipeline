FROM openjdk:8
EXPOSE 8080
ADD target/spring-boot-k8S-0.2.0.jar cicd-demo-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/cicd-demo-0.0.1-SNAPSHOT.jar"]